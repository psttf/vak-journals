pdftotext initial.pdf generated/initial.txt
sed -i -E ':a;N;$!ba;s/\n/%/g' \
     generated/initial.txt
sed -E 's/%+([[:alnum:]]{4}-[[:alnum:]]{4})\,{,1}%+/\n\1%/g' \
    generated/initial.txt \
    > generated/specs.txt
grep "2\.3\.5\.[ %]Математическое[ %]и[ %]программное[ %]обеспечение[ %]вычислительных[ %]систем,[ %]комплексов[ %]и[ %]компьютерных[ %]сетей[ %](технические[ %]науки)" \
    generated/specs.txt \
    > specs_2_3_5.txt
sed -i -E 's/%.*//g' \
    specs_2_3_5.txt
sed -E 's/((%+[^[:digit:]%]+)*%+[^%]+)%+([[:alnum:]]{4}-[[:alnum:]]{4})\,{,1}%+/\n%%%journal%%%\1%\3%/g' \
    generated/initial.txt \
    > names.txt
sed -i -E 's/%+[[:digit:]]+(\.| ).*//g' \
    names.txt
grep "%%%journal%%%" \
     names.txt \
     | sponge names.txt
sed -i -E 's/%+journal%+//g' \
    names.txt
sed -i -E 's/^(.*)%([[:alnum:]]{4}-[[:alnum:]]{4})(.*)/\2|\1\3/g' \
    names.txt
sed -i -E 's/%/ /g' \
    names.txt
sed -i 's/\r$//' \
    names.txt
